/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 1;       // border pixel of windows
static const unsigned int snap      = 32;      // snap pixel
static const int showbar            = 1;       // 0 means no bar
static const int topbar             = 1;       // 0 means bottom bar
static const int topbarheight       = 21;      /* 0 means automatic; else it  *
                                                * will use user specified     *
                                                * height; 21 is just a little *
                                                * bigger than default and is  *
                                                * nice.                       */
static const int verticalbarpadding   = 5;     // vertical bar padding
static const int horizontalbarpadding = 5;     // horizontal bar padding
static const int gapsize              = 5;     // size of gaps between windows

static const char *fonts[]     = { "monospace:size=10" };
static const char dmenufont[]  = "monospace:size=10";

static const char col_cyan[]   = "#005577";

static const char col_gray0[]  = "#111111";
static const char col_gray1[]  = "#222222";
static const char col_gray2[]  = "#444444";
static const char col_gray3[]  = "#bbbbbb";
static const char col_gray4[]  = "#eeeeee";
static const char col_green0[] = "#00b800";

/* All the Colours of the Rainbow */
/*
static const char col_red[]    = "#ff0000";
static const char col_orange[] = "#ff7f00";
static const char col_yellow[] = "#ffff00";
static const char col_green[]  = "#00ff00";
static const char col_blue[]   = "#0000ff";
static const char col_indigo[] = "#4b0082";
static const char col_violet[] = "#9400d3";
static const char col_black[]  = "#000000";
*/
static const char col_white[]  = "#ffffff";

static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_white, col_gray0, col_gray0 },
	[SchemeSel]  = { col_white, col_gray2, col_green0 },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const char *tagsel[][2] = {
	/* fg color, bg colour  */
	{ col_white, col_gray2  }, // grey2
	{ col_white, col_gray2  }, // grey2
	{ col_white, col_gray2  }, // grey2
	{ col_white, col_gray2  }, // grey2
	{ col_white, col_gray2  }, // grey2
	{ col_white, col_gray2  }, // grey2
	{ col_white, col_gray2  }, // grey2
	{ col_white, col_gray2  }, // grey2
	{ col_white, col_gray2  }, // grey2
/*
	{ col_white, col_red    }, // Red
	{ col_white, col_orange }, // Orange
	{ col_black, col_yellow }, // Yellow
	{ col_black, col_green  }, // Green
	{ col_white, col_blue   }, // Blue
	{ col_white, col_indigo }, // Indigo
	{ col_white, col_violet }, // Violet
	{ col_black, col_white  }, // White
	{ col_white, col_black  }, // Black
*/
};

/* xprop(1):                                                          *
 *	WM_CLASS(STRING) = instance, class                            *
 *	WM_NAME(STRING) = title                                       *
 *	                                                              *
 * isfloating:                                                        *
 *      0 = tiled                                                     *
 *      1 = floating                                                  */
static const Rule rules[] = {
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       0,            0,           -1 }, // GIMP doesn't need to float anymore
	{ "Firefox",  NULL,       NULL,       1 << 8,       0,           -1 },

	{ "Steam",
	              NULL,
	              "Steam - News",
	              1 << 0,
	              1,
	             -1
	},
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile                   }, // first entry is default
	{ "flt",      NULL                   }, // no layout function means floating behavior
	{ "[M]",      monocle                },
	{ "|||",      threecolumn            }, // "GIMP" Layout
	{ "|M|",      centeredfloatingmaster }, // Centered floating master
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-b", "-fn", dmenufont, "-nb", col_gray0, "-nf", col_white, "-sb", col_cyan, "-sf", col_white, NULL };
static const char *termcmd[]  = { "st", "-e", "tmux", "-u", NULL };

static Key keys[] = {
	/* modifier                     key        function          argument */
	// SPAWN COMMANDS
	{ MODKEY,                       XK_p,      spawn,            {.v = dmenucmd } },
	{ MODKEY|ShiftMask,             XK_Return, spawn,            {.v = termcmd } },
	// (UN)HIDE TOP BAR
	{ MODKEY,                       XK_b,      togglebar,        {0} },
	// REFOCUS CLIENTS
	{ MODKEY,                       XK_j,      focusstack,       {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,       {.i = -1 } },
	// INCREASE OR DECREASE NUMBBER OF MASTERS
	{ MODKEY,                       XK_i,      incnmaster,       {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,       {.i = -1 } },
	// RESIZE STACK WIDTHS
	{ MODKEY,                       XK_h,      setmfact,         {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,         {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_h,      setmfact,         {.f = -0.001} },
	{ MODKEY|ShiftMask,             XK_l,      setmfact,         {.f = +0.001} },
	// CFACTS
	{ MODKEY|ControlMask,           XK_h,      setcfact,         {.f = +0.25} },
	{ MODKEY|ControlMask,           XK_l,      setcfact,         {.f = -0.25} },
	{ MODKEY|ControlMask|ShiftMask, XK_h,      setcfact,         {.f = +0.125} },
	{ MODKEY|ControlMask|ShiftMask, XK_l,      setcfact,         {.f = -0.125} },
	{ MODKEY,                       XK_o,      setcfact,         {.f =  0.00} },
	// MOVE SLAVE TO MASTER
	{ MODKEY,                       XK_Return, zoom,             {0} },
	// SWITCH TO PREVIOUS TAG
	{ MODKEY,                       XK_Tab,    view,             {0} },
	// KILL CLIENT, KILL WINDOW MANAGER
	{ MODKEY|ShiftMask,             XK_c,      killclient,       {0} },
	{ MODKEY|ShiftMask,             XK_q,      quit,             {0} },
	// LAYOUTS (ALL WINDOWS)
	{ MODKEY,                       XK_t,      setlayout,        {.v = &layouts[0]} },
	{ MODKEY,                       XK_space,  setlayout,        {0} },
	{ MODKEY|ShiftMask,             XK_f,      setlayout,        {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,        {.v = &layouts[2]} }, // Sets Monocle
	{ MODKEY,                       XK_u,      setlayout,        {.v = &layouts[3]} }, // Sets 3 Column
	{ MODKEY|ShiftMask,             XK_u,      setlayout,        {.v = &layouts[4]} }, // Sets centered Master
	// INDIVIDUAL WINDOW STATES
	{ MODKEY|ShiftMask,             XK_space,  togglefloating,   {0} },
	{ MODKEY,                       XK_f,      togglefullscreen, {0} },
	// GAPS
	{ MODKEY,                       XK_minus,  setgaps,          {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,          {.i = +1 } },
	{ MODKEY,                       XK_equal,  setgaps,          {.i =  0 } },
	// TAG OPTIONS
	{ MODKEY,                       XK_0,      view,             {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,              {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,         {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,         {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,           {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,           {.i = +1 } },
	TAGKEYS(                        XK_1,                        0)
	TAGKEYS(                        XK_2,                        1)
	TAGKEYS(                        XK_3,                        2)
	TAGKEYS(                        XK_4,                        3)
	TAGKEYS(                        XK_5,                        4)
	TAGKEYS(                        XK_6,                        5)
	TAGKEYS(                        XK_7,                        6)
	TAGKEYS(                        XK_8,                        7)
	TAGKEYS(                        XK_9,                        8)
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

