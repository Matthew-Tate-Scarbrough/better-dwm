<!-- 
  AUTHOR:  Matþew T. Scarbrough
  CREATED: 14 August 2021
  VERSION: v1.00d
  REVISED: 06 October 2022
  NOTES:   Tabstop = 4
           Linewidth = 72
  vim: colorcolumn=72 tabstop=4 softtabstop=4 shiftwidth=4 expandtab
  -->

<a name="about"></a>
# "Better" DWM

!["Better" DWM, a dwm fork with sensible defaults](Pictures/dwm1.png)

"Better" DWM is my fork of the Suckless dynamic-window manager (`dwm`).
It is called `"Better DWM"` because it is _subjectively_ better, not
objectively.  It is manually patched with the ones available on the
[Suckless website](https://dwm.suckless.org/Patches) which I think are
necessary.  Many of the patches do have minor modifications, such as
renamed variables (to ensure they match the style used in the rest of
the dwm source code). Go through the commit history to find diffs of
the patches I have applied.  Do note, though, that many patches do
earlier patches. Customization of the dwm look and keybindings is not
much different than the default.

This particular fork was built on
[Void Linux](https://docs.voidlinux.org/) [MUSL](https://musl.libc.org/)
(x86/amd64), though tested on
[Devuan 3.0](https://www.devuan.org/),
[FreeBSD 13.0](https://www.freebsd.org/),
and
[OpenBSD 6.9](https://www.openbsd.org/).

<a name="toc"></a>
## Contents

**BEFORE READING**

Because the process is so straight forward, know that the included
building instructions are meant for absolute beginners.  As such, to
balance explicit explanation with minimalism (to avoid redundancy but
not leave anything out), if you are a beginner, start at the Void Linux
instructions and read through them as they are the most exhaustive and
explicit at explaining what is going on.

This is a representation of how the whole README is laid out.  It is
meant to introduce new, relevant information as quickly as possible,
avoid redundant information, and address questions as they come up.  The
TODO section, however, breaks this.  It is meant to be quickly glanced
at by someone who is already familiar with dwm and this fork.

  + [About](#about)
  + [TODO](#todo)
  + [What is a Window Manager](#what-is)
  + [Features](#features)
  + [Guide](#how-to)
    - [What are Stacks](#about-stacks)
    - [How to Navigate the Stack](#navigating-stacks)
    - [What are Tags](#about-tags)
    - [How to Navigate Tags](#navigating-tags)
    - [What are Workspaces](#about-monitors)
    - [How to Navigate Workspaces](#navigating-monitors)
  + [Features not Planned](#out-of-scope)
  + [Building](#building)
    - [Building on Linux](#building-linux)
      * [Void Linux](#building-void)
      * [Debian](#building-debian)
    - [Building on BSD](#building-bsd)
      * [FreeBSD](#building-fbsd)
      * [OpenBSD](#building-obsd)
  + [Applied Patches](#patches)
  + [Off-limits Patches](#banned-patches)


<a name="todo"></a>
## TODO

The following is a todo list ranked in order of importance, not in
likelihood of happening.

  1. Add a way of reparenting windows
     - see [Off-limits Patches: #1](#banned-patches)
  2. Fix gap issues:
     - fix issue with Monocle gaps not resizing;
     - add gaps to:
       * Floating Centered Master layout,
       * Three-column Layout.
  3. Clean-up `config.def.h`
  4. Make source easier to read
     - Comments;
     - Explanations of functions;
     - Separate non-essential functions into their own files
       i.e. _layout functions_.

<a name="what-is"></a>
## What is a (Tiling) Window Manager?

A _window manager_ (WM) is a program that is dedicated to spawning
graphical clients and managing their positions.  A _floating_ WM is one
that does not force any specific parameters on a client in any way,
shape, or form.  By contrast, a _tiling_ WM does.  They force windows to
spawn in specific, predictable shapes and patterns and cannot handle
floating windows.  Dwm, by contrast, is a _dynamic_ window manager,
meaning it combines both feature-sets.

A WM is almost always keyboard-driven and lightweight.  Their job is to
do what they need to only---manage windows. Typically, animations,
VSync, opacity, etc. are handled by a separate program, called a
_compositing window manager_ (_compositor_ for short).  _Picom_ is
probably the most well-known compositor for X.  On Wayland (the
successor to X), a _compositor_ combines both the features of a window
manager and an X compositor.

As dwm is built for X, it cannot be used with Wayland and must be used
with a separate compositor.


<a name="features"></a>
## Features

This fork has a number of features.  The following is a list of the
_useful_ ones.  Most of these are not unique to this fork.

  + Multiple layouts:
    - Tiling(default)
      * Clients can be vertically resized,
    - Monocle,
    - Three-column (the "GIMP-layout"),
    - Floating Centered Master Layout,
    - Floating,
    - Fullscreen;
  + Tags instead of workspaces:
    - unique layouts can be chosen per tag,
    - each tag indicator can have a unique colour,
    - each tag indicator is hidden unless a client is open on it;
  + Multiple monitor support.


<a name="guide"></a>
## How to

**Foreword**

This is a short guide on how to use dwm.  It is recommend that you get
moderately acquainted with Vim first.  You may do this by installing
`vim-tutor`.  It will probably be better, however, to play
[Vim Adventures](https://vim-adventures.com) instead.  This is because
dwm, by default use _Vim-like_ keybindings.  If it did not, I would have
remapped them to being Vim-like.  For more complete information about
dwm, see `dwm(1)`, using:

    man 1 dwm

(yes, the number in round brackets is the number after man---it stands
 which _volume_ of the `man`ual you are searching through.)  This guide,
however, will offer a unique advantage to the manual page.  It will give
you two very useful modifications you can add to `config.def.h`.

<a name="about-stacks"></a>
**WHAT IS A STACK?**

Before you can learn the specifics, you need to understand what a 
_stack_ is.  A stack is a group of windows.  So named because the
clients appear as vertically stacked, like this:

    +--------------------------------------------------+ --+
    |                                                  |   |
    |                                                  |   |
    |                     Client 1                     |   |
    |                                                  |   |
    |                                                  |   |
    +--------------------------------------------------+   |
    |                                                  |   |
    |                                                  |   |
    |                     Client 2                     |   +-- Stack
    |                                                  |   |
    |                                                  |   |
    +--------------------------------------------------+   |
    |                                                  |   |
    |                                                  |   |
    |                     Client 3                     |   |
    |                                                  |   |
    |                                                  |   |
    +--------------------------------------------------+ --+

With advanced layouts like
[Fibonacci](http://dwm.suckless.org/patches/fibonacci/) (not included)
three-column ("GIMP"), the model is not visual, it is conceptual.  There
is still a stack---a hierarchy based off of which is first and which is
last in sequence---it just isn't vertical.  In natural language we have
mane words to describe different patterns.  A _column_ is a vertical
stack.  A _row_ is a horizontal stack.  A _line (american)_ or _queue
(England)_ is a long-stack and so forth.  Just note that with dwm, the
term _stack_ is this open-ended---it doesn't mean one thing.

Dwm uses _two_ stacks.  The main stack (the big one), which is called
the _master_ stack, and the secondary stack, called the _slave_ stack.
They are also called the _master_ and _slave area_.  The master is
intended to be your primary working area.  The slave is where you are
meant to store presently unused clients.  You may use them however you
like and there is even a patch,
[focusmaster](https://dwm.suckless.org/patches/focusmaster/), which will
automatically _zoom_ clients as you scroll through the stacks.  Zoom
means to move a client from the slave area to the master.  You can even
mess with `dwm.c` to move the master to the right-side, as some like.

To give a more clear example, you can test spawning terminal instances
out with `<Shift>+<Mod>+<Return`, which will spawn an
[st](https://st.suckless.org/) instance every time you execute it.  You
may desire to change the `termcmd` function to spawn your preferred
terminal, such as:
[Alacritty](https://github.com/alacritty/alacritty);
[Kitty](https://github.com/kovidgoyal/kitty);
[Xterm](https://invisible-island.net/xterm/);
etc.

```
    - static const char *termcmd[] = { "st",   NULL };
    + static const char *termcmd[] = { "gnome-terminal", NULL };
```

If you intend on using mostly Vim or EMACS, you can create your own
function like this:

    static const char *editorcmd[] = { "kitty", "-e", "emacs", NULL };
    static Key keys[] = {
        { MODKEY,    XK_e,    spawn,    {.v = editorcmd } },
    };

This will spawn an instance of Kitty, passing to it the flag `-e`, which
tells it to run the argument `emacs`.

`MOD` is Left-Super, also called the "meta-key" or the "Win\[dows\]"
key.

You can now use compile dwm and test these out real-time.  Or refer to
this boring map:

                    Master                            Slave
                      |                                 |
    +-----------------+-----------------+---------------+--------------+
    |                                   |                              |
                                       
    +-----------------------------------+------------------------------+
    |                                   |                              |
    |                                   |                              |
    |                                   |                              |
    |                                   |           Client 2           |
    |                                   |                              |
    |                                   |                              |
    |                                   |                              |
    |             Client 1              +------------------------------+
    |                                   |                              |
    |                                   |                              |
    |                                   |                              |
    |                                   |           Client 3           |
    |                                   |                              |
    |                                   |                              |
    |                                   |                              |
    +-----------------------------------+------------------------------+

With `<MOD>+<i/d>`, you can `i`ncrement or `d`ecrement the number of
masters.  If you press `MOD+<i>`, thin you will increase the number of
masters to two:

    +-----------------------------------+------------------------------+
    |                                   |                              |
    |                                   |                              |
    |                                   |                              |
    |             Client 1              |                              |
    |                                   |                              |
    |                                   |                              |
    |                                   |                              |
    +-----------------------------------+           Client 3           |
    |                                   |                              |
    |                                   |                              |
    |                                   |                              |
    |             Client 2              |                              |
    |                                   |                              |
    |                                   |                              |
    |                                   |                              |
    +-----------------------------------+------------------------------+

If you, at this state with two masters, press `<MOD>+<d>` twice, you
will decrease the number of masters to none:

    +------------------------------------------------------------------+
    |                                                                  |
    |                                                                  |
    |                             Client 1                             |
    |                                                                  |
    |                                                                  |
    +------------------------------------------------------------------+
    |                                                                  |
    |                                                                  |
    |                             Client 2                             |
    |                                                                  |
    |                                                                  |
    +------------------------------------------------------------------+
    |                                                                  |
    |                                                                  |
    |                             Client 3                             |
    |                                                                  |
    |                                                                  |
    +------------------------------------------------------------------+

Anymore will create a black hole and crash dwm.  (No it won't.)

<a name="navigating-stacks"></a>
**MOVING AROUND THE STACK**

You move through the stack using `<MOD>+<j/k>`.

  + `<MOD>`+`<j>` moves _right_ through the stack;
  + `<MOD>`+`<k>` moves _left_ through the stack.

An alternative way of thinking about this is down/up.  The point is:
_advancing_ and _regressing_ through the stack.  Moving forward and
backward.  If you are focused on Client 1, then `<MOD>+<j>` will focus
on Client 1+1, which is 2; and 2+1, which is 3, and so forth.  It will
loop around.  Take this example in which the Master is focused:

    <MOD>+<j>
    ---------
    
    ┏━━━━━━━━━━━━━━━┱─────────────┐      ┌───────────────┲━━━━━━━━━━━━━┓
    ┃               ┃             │      │               ┃             ┃
    ┃               ┃      2      │      │               ┃      2      ┃
    ┃               ┃             │      │               ┃             ┃
    ┃       1       ┠─────────────┤  ->  │       1       ┡━━━━━━━━━━━━━┩
    ┃               ┃             │      │               │             │
    ┃               ┃      3      │      │               │      3      │
    ┃               ┃             │      │               │             │
    ┗━━━━━━━━━━━━━━━┹─────────────┘      └───────────────┴─────────────┘

And here is an example of pressing down four times on the Fibonacci
layout (not included):

    4<MOD>+<j>
    ----------
    
    ┏━━━━━━━━━━━━━┱───────────────┐      ┌─────────────┬───────────────┐
    ┃             ┃               │      │             │               │
    ┃             ┃       2       │      │             │       2       │
    ┃             ┃               │      │             │               │
    ┃      1      ┠───┬───┬───────┤  ->  │      1      ┢━━━┱───┬───────┤
    ┃             ┃ 5 │ 6 │       │      │             ┃ 5 ┃ 6 │       │
    ┃             ┠───┴───┤   3   │      │             ┡━━━┹───┤   3   │
    ┃             ┃   4   │       │      │             │   4   │       │
    ┗━━━━━━━━━━━━━┹───────┴───────┘      └─────────────┴───────┴───────┘

Pressing `<MOD>+<Return>` will zoom in on the highlighted client:

    <MOD>+<Return>
    --------------
    
    ┌─────────────┬───────────────┐    ┏━━━━━━━━━━━━━┱───────────────┐
    │             │               │    ┃             ┃               │
    │             │       2       │    ┃             ┃       2       │
    │             │               │    ┃             ┃               │
    │      1      ┢━━━┱───┬───────┤ -> ┃      5      ┠───┬───┬───────┤
    │             ┃ 5 ┃ 6 │       │    ┃             ┃ 1 │ 6 │       │
    │             ┡━━━┹───┤   3   │    ┃             ┠───┴───┤   3   │
    │             │   4   │       │    ┃             ┃   4   │       │
    └─────────────┴───────┴───────┘    ┗━━━━━━━━━━━━━┹───────┴───────┘

You can change between the four most important layouts thusly:

  + `<MOD>`+`<f>` --- Fullscreen
  + `<MOD>`+`<m>` --- Monocle
  + `<MOD>`+`<t>` --- Tiled

If you hold down mod, left-click, then drag the mouse on a client, it
will become floating:

    <MOD>+<Mouse1>+<Drag to top-left>
    ---------------------------------
    
    ┌─────────────┬───────────────┐    ┌─────────────┬───────────────┐
    │             │               │    │       ┏━━━━━━━┓             │
    │             │       2       │    │       ┃       ┃     2       │
    │             │               │    │       ┃   5   ┃             │
    │      1      ┢━━━┱───┬───────┤ -> │      1┃     ↑ ┃─────┬───────┤
    │             ┃ 5↑┃ 6 │       │    │       ┗━━━━━━━┛ 6   │       │
    │             ┡━━━┹───┤   3   │    │             ├───────┤   3   │
    │             │   4   │       │    │             │   4   │       │
    └─────────────┴───────┴───────┘    └─────────────┴───────┴───────┘

_NOTE:_ Does **not** resize.  That is `<MOD>+<Mouse2>`.

In order to switch a client back to being tiled from floating, you press
`<MOD>+<Space>`.  This will actually toggles between floating and
tiling.

<a name="about-tags"></a>
**WHAT IS A TAG?**

Tags are not workspaces.  A workspace is a virtual monitor.  And a
Monitor in dwm is basically a workspace.  A tag is best thought of as a
file that stores a client and its position on screen.  You can show
multiple tags at the same time without overwriting the location and size
information of any individual tag.  Essentially, when one tag is shown,
all other windows on all other tags are minimized.

A way of thinking about this is that a folder in a cabinet is the
monitor or workspace.  And the pieces of paper with drawings or writings
inside the folder are the tags.  You can pull two pieces of paper out
and look at them at the same time, or you can cut out bits of
information, such as a paragraph, and glue it to another piece of paper.
The only difference is, when you stop viewing both tags, all clients go
back to where they were.  This is called _pinning_---the only "real"
difference from a workspace.

If you are familiar with AutoCAD, a tag is like a "view" of the drawing.

Because of how this fork of dwm has been patched, tags will store
whether the bar is shown or hidden and which layout is selected.  By
default, dwm proper will not do this.

<a name="navigating-tags"></a>
**MOVING AROUND TAGS**

There are nine tags altogether.  You focus on each tag by pressing
`<MOD>+<1-9>`.  Default dwm will show all 9.  This fork will show them
only as you switch to them or as you pin or open clients on them.  In
the top of each tag, there is a little box.  This shows whether there
are clients open on that tag.

By pressing `<MOD>+<0>` (super+zero), you view all clients on all tags
at the same time.  This will not mess with the active layout of each tag
and dwm will not crash if all tags have different layouts.  It functions
as if it has opened a tenth tag which has all clients of that monitor
(workspace) pinned to it and can have its own unique layout set to it
without messing with the other tags' layouts.

If you press `<Shift>+<MOD>+<1-9>`, you will send an active client to
another tag.  Workspaces can do the same thing.

If you press `<Ctrl>+<MOD>+<1-9>`, you will open two tags or views at
the same time.  Again, this will not cause any problems or change any
layouts of any tags.  Each tag keeps its own layout and the newly viewed
windows will assume whatever layout of the tag to which you are pinning
another's clients.

If you press `<Ctrl>+<Shift>+<MOD>+<1-9>`, you will pin whichever client
is currently focused to that tag you specified.  Workspaces cannot do
this.

If you press `<Shift>+<MOD>+<0>` (again, 'zero'), you will pin the
presently focused client to all tags.  This is useful for a webcam view
or perhaps a media player for a movie or TV show you legitimately
obtained.  Legitimately obtained from a shady site illegitimately, that
is.

<a name="about-monitors"></a>
**WHAT ARE WORKSPACES?**

As previously mentioned dwm does not "use" workspaces.  A workspace is
merely a virtual monitor.  So therefore dwm's workspaces are multiple
monitors.  There is no need to have workspaces as tags are better in
absolutely every way.  By default, dwm cannot pin a client to another
workspace.

<a name="navigating-monitors"></a>
**MOVING AROUND WORKSPACES**

To move to another workspace, use `<MOD>+<,/.>`.  These will decrement
and increment the monitor selection.

`<Shift>+<MOD>+<,/.>` will move the currently highlighted client to the
previous or next monitor.

**CONCLUSION**

Tags are better than Workspaces.  And this means that dwm is better than
every WM or desktop environment (DE) that does not have them.  Basically
better than everything except Herbstluft WM and Awesome WM.

<a name="out-of-scope"></a>
## Features not Presently Considered

The following are features not currently considered to be added to this
fork of dwm.  More may be added in time.  Alpha is a candidate, though
it is unnecessary, in my opinion, if gaps are used.  The point in both
is to see the background.  It is worth mentioning, though, that alpha is
unnecessary for transparency of _clients_.  That is what a compositor is
for.

  + Alpha;
  + Multiple bars;
  + Built-in system-tray.


- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


<a name="building"></a>
## Building

**General Introduction**

Customization of dwm is done at compile-time via a config file, called
`config.h`.  If it does not exist, it will be created at compile time
from the default configuration file, called `config.def.h`.  You may
freely copy it using the following command:

    cp config.def.h config.h

You need to have a basic understanding of C, but the syntax is simple
and there should be plenty of examples in the config file.

If you are compiling on OpenBSD or FreeBSD, you will need to modify the
`config.mk` (not to be confused with `config.h`) file to match the
system in question, due to different libraries and library locations.
The instructions are both here and in `config.mk`.

You may compile dwm using the following command:

    make -j $(nproc)

Only `make` is necessary.  But the flag `-j` will tell `make` to use
more than one CPU core or thread.  `$( ... )` is a shell syntax that
will be replaced with the output of any command put inside the round
brackets \[()\].  `nproc` will output the number of cores or threads
available.

You may install dwm with the following command:

    sudo make install

If dwm is already installed, this command will reinstall it.

**Hunting for Packages**

When you compile dwm initially there may be issues.  You may see red or
bold coloured error codes.  In most cases, these are going to be missing
libraries that your OS needs to build dwm.

You will need:

  + Tools for building,
  + Git to clone dwm, and
  + Xorg development tools:
    - development files for Xorg server;
	- development files for the Xft font rendering library; and
	- development files for Xinerama, which allows for the same image to
	  be stretched across multiple monitors.

Some of these tools may be only available in _meta packages_---FreeBSD
and OpenBSD are a fine example of this in that on them, if you install
"Xorg" (not the meta package name) on them, then it installs everything,
which is nice.  A meta package is a symbolic name for a group of
packages.

**Running dwm**

To run it, you must _at least_ make the file `~/.xinitrc`.  This can be
done via:

    echo "exec dwm" > ~/.xinitrc

or

    printf "exec dwm\n" > ~/.xinitrc

You may then run it with the command:

    startx

But you most likely will want to run dwm with _dbus_ (see
`dbus-daemon(1)`).  This would require enabling the dbus daemon at
start-up, then creating `~/.xinitrc` with this line instead:

    echo "dbus-run-session dwm" > ~/.xinitrc

See the Suckless [README](README) or the dwm Arch wiki
[page](https://wiki.archlinux.org/title/Dwm#Statusbar_configuration) to
find out more information on what all `~/.xinitrc` is used for dwm-wise.
Else search the [OpenBSD FAQ](https://www.openbsd.org/faq/index.html) to
find out more information relevant.

_NOTE:_ If you use Firefox or Nautilus or a GTK or KDE application and
        you find they take longer than they should to load, this is
        because you need dbus functionality, so this step is not
        optional.

<a name="building-linux"></a>
## Linux

Building on Linux is easiest.  You need only install the proper
development libraries.  Here are the instructions for the most common
distros:

<a name="building-void"></a>
**Void Linux**

To build dwm:

    xbps-install base-devel git xorg-server-devel libXft-devel
                 libXinerama-devel

_NOTE:_ It is worth noting that if you try and `make` dwm and it throws
        up error messages in red, these are telling you what is wrong.
        It can be scary at first, but if you calm down, take a breath,
        and empty your mind, they make a lot of sense.  For instance,
        the message saying "something `Xft.h`", it wants you to find the
        development header file for Xft.  This is `libXft-devel`.

To run dwm:

    xbps-install xorg-minimal xorg-fonts

The first is a meta package that contains the most common X utilities
and the xserver itself as well as programs to launch applications.  The
second is, as the name implies, a meta package for some X-compatible
fonts.  If X's fontcache is empty, it will not launch.

The packages are predictably similar across other distributions and
\*NIX OS's (i.e. FreeBSD and OpenBSD).


<a name="building-arch"></a>
**Arch** (Why Pacman is good: it is simple.
          Why Pacman is bad:  it is simple.)

Like Void but with `pacman -S` instead, slightly more confusing (though
not as bad as Debian) package names, and a necessity to do a full-system
upgrade bi-weekly or suffer the consequences.

Consult the Arch-Wiki.

<a name="building-debian"></a>
**Debian** (Pronounced like _Debian_, not _Debian_.  _Ian_'s wife's name
            is ***Deb***_by_.)

_NOTE:_ Tested on Devuan.

Build:

    apt install build-essential git xorg-xserver-dev libxft-dev
                libxinerama-dev

Run:

    apt install xserver-xorg-core xinit

<a name="building-bsd"></a>
## BSD's

<a name="building-fbsd"></a>
**FreeBSD**

To build and run:

First, you must comment and uncomment the appropriate lines in
`config.mk`.  This is because the compiler needs to know where to find
the appropriate header files.  Because FreeBSD is so well organized,
they are all in one place.

You must comment lines `11`, `12`, and `25`.  Then uncomment lines `15`,
`16`, and `27`.

    pkg install xorg-minimal libXft libXinerama xorg-fonts

FreeBSD always includes development files as well.  I better stop before
I simp for FreeBSD.

<a name="building-obsd"></a>
**OpenBSD** (The most secure OS by default, _sukkit_ Qubes... you are
             secure in theory, though.)

Comment lines `25` in `config.mk`, then uncomment line 29.

    make
    doas make install

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


<a name="patches"></a>
## Patches

The following is a list of patches applied to dwm in order of
importance.  In parentheses is the order of application.

  + [Actualfullscreen](http://dwm.suckless.org/patches/actualfullscreen/dwm-actualfullscreen-20191112-cb3f58a.diff) (5)
    - By default, something possessed the dwm devs to write dwm so that
      it cannot do an toggle fullscreen.  Instead, you are recommended
      to use monocle and hide the top bar.  This is no solution and will
      break some apps.  This patch allows dwm to actually toggle
      fullscreen independent of the program that needs it.
  + [Pertag](http://dwm.suckless.org/patches/pertag/dwm-pertag-20200914-61bb8b2.diff) (8)
    - While this does a lot, the only thing pertinent is that it:
      1. Remembers if the layout you have chosen per tag (rather than
         per monitor), and
      2. Remembers whether you have hidden the top bar on a monitor or
         not.
  + [Sendmon Keepfocus](https://github.com/bakkeby/patches/blob/master/dwm/dwm-sendmon_keepfocus-6.2.diff) (9)
    - Unfortunately named, this patch will, upon sending a client to
      another monitor, refocus on the sent-client.
  + [Cfacts](http://dwm.suckless.org/patches/cfacts/dwm-cfacts-20200913-61bb8b2.diff) (12)
    - Resizes clients in the tile layout only.
  + [Cyclelayouts](http://dwm.suckless.org/patches/cyclelayouts/dwm-cyclelayouts-20180524-6.2.diff) (15)
    - Cycles through tiling layouts with mouse wheel and keybindings.
  + [custom urgent border](https://dwm.suckless.org/patches/urgentborder/dwm-6.2-urg-border.diff) (18)
    - Change urgent borders to a custom colourscheem (gruvbox orange by default)
  + [Shiftview](https://lists.suckless.org/dev/att-7590/shiftview.c) (16)
    - Cycles through tiling tags with mouse wheel and keybindings.
	- Function renamed to `cycletag`.
  + [Three Column](http://dwm.suckless.org/patches/three-column/tcl.c) (9)
    - This patch can either be written as was originally intended--as an
      separate file that is then `#include`-d in or put directly into
      `dwm.c`.
  + [Floating Centered Master](http://dwm.suckless.org/patches/centeredmaster/dwm-centeredmaster-6.1.diff) (13)
    - Only the floating version, not the threecolumn layout.
  + [Autoresize](http://dwm.suckless.org/patches/autoresize/dwm-autoresize-20160718-56a31dc.diff) (4)
    - Allows unfocused clients that want a window resize to resize.
  + [Attachbottom](http://dwm.suckless.org/patches/attachbottom/dwm-attachbottom-20201227-61bb8b2.diff) (2)
    - Moves all newly spawned clients, by default, to the bottom of the
      slave stack.
  + [Alwayscenter](https://dwm.suckless.org/patches/alwayscenter/dwm-alwayscenter-20200625-f04cac6.diff) (14)
    - Moves all floating windows to center of screen at spawn.
	- Does not prevent moving or resizing.
  + [Active Tag Indicator](http://dwm.suckless.org/patches/activetagindicatorbar/dwm-activetagindicatorbar-6.2.diff) (1)
    - Each tag's icon can have a dot in their top left corner.  This dot
      shows as an outline on tags that are not active but have open
      Clients.  It shows as a solid colour on tags that have an active
      open Client.  This patch changes the dot into a rectangle, which
      is much easier to see and visually pleasing.
  + [Winicon](https://dwm.suckless.org/patches/winicon/dwm-winicon-6.2-v2.1.diff) (17)
    - Add icons before the Window title in the top bar
  + [Bar Height](http://dwm.suckless.org/patches/bar_height/dwm-bar-height-6.2.diff) (3)
    - Optionally change the height of the top-bar.
  + [Rainbowtags](http://dwm.suckless.org/patches/rainbowtags/dwm-rainbowtags-6.2.diff) (10)
    - Allows tags to be different colours.
  + [Fullgaps](http://dwm.suckless.org/patches/fullgaps/dwm-fullgaps-toggle-20200830.diff) (7)
    - I modified the patch, albeit poorly, to also add gaps around the
      `monocle` layout; monocle is not a replacement for an actual
      fullscreen feature.
      
      My modification is flawed, though it does not break dwm but.  It
      does not work as expected.  It was intended to add gaps and allow
      them to be dynamically scaled like the tiling layout.  They are
      fixed at startup:

```diff
      @@ -1131,7 +1158,11 @@ monocle(Monitor *m)
        if (n > 0) /* override layout symbol */
                snprintf(m->ltsymbol, sizeof m->ltsymbol, "[%d]", n);
        for (c = nexttiled(m->clients); c; c = nexttiled(c->next))
-               resize(c, m->wx, m->wy, m->ww - 2 * c->bw, m->wh - 2 * c->bw, 0);
+               if (selmon->gapsize)
+                       //resize(c, m->wx, m->wy, m->ww - 2 * c->bw, m->wh - 2 * c->bw, 0);
+                       resize(c, m->wx + gapsize, m->wy + gapsize, m->ww - 2 * gapsize, m->wh - 2 * gapsize, 0);
+               else
+                       resize(c, m->wx - c->bw, m->wy, m->ww, m->wh, False);
 }
```

  + [Barpadding](http://dwm.suckless.org/patches/barpadding/dwm-barpadding-20200720-bb2e722.diff) (6)
    - Adds padding around left and right sides of the bar as well as
      the top of the bar.
    - Does not add padding to bottom of bar.  This is so as to not
      interfere with any patches that add gaps.
    - If the bar is moved to the bottom of the page, the padding is
      beneath the bar as it should be, not above.
  + [Hide Vacant Tags](http://dwm.suckless.org/patches/hide_vacant_tags/dwm-hide_vacant_tags-6.2.diff) (11)
    - Kept active tag indicator, though.


<a name="banned-patches"></a>
## Off-limits (Dangerous) Patches

This is a list of patches that under no circumstances shall be added to
this fork of dwm.  This is because they have been found to have issues
that break dwm.  As such, they are _dangerous_.

  + [Awesomebar](http://dwm.suckless.org/patches/awesomebar/dwm-awesomebar-20200907-6.2.diff)
    - Though written for an older version of dwm, changing the `sw`
      variable to `tw`, and changing another line will allow the patch
      to function mostly as expected and compile fine, the patch will
      cause dwm to crash.  Conditions under which the crash occurs:
      1. If a single client is open on a tab and this client is hidden,
         if you then attempt to scroll to another (non-existent) client
         without showing the client, then dwm will crash;
      2. If and only if you have a client open on any tag on any monitor,
         if you then click on the (empty) clients portion of the bar, dwm
         will crash.
